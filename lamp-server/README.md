# How to create the lamp-server package ?









## Plan 1
### Lamp-server(package)
- Installing dependencies (apache php adminer mariadb phpmyadmin)
- Configuring the apache with the below things
- Add a2enmod support to apache server
- Include phpMyAdmin and Adminer config to apache config
- Modify php config
- Setting up mariadb database i.e., Create a user and password with privileges
- Making this as a package called lamp-server











## Plan 2
### Modified apache (package)
- Instead of installing apache, modify it as new package with php support
- Adding a2enmod support
- The modified package will be comes with support for php, phpMyAdmin and adminer
- Configuring the apache server with the above things

### Lamp-server(package)
- Instead of installing the default apache package, install the mod version
- Installing dependencies (apache(mod) php adminer mariadb phpmyadmin)
- Setting up mariadb database i.e., Create a user and password with privileges
- Making this as a package called lamp-server