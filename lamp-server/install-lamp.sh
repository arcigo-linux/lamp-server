#!/bin/bash

# Title	   	   : Install LAMP stack
# Purpose	   : Install LAMP (Linux Apache MYSQL PHP) server in ArchLinux based distros.
# Author	   : Henry
# Email		   : arcigo.linux@gmail.com
# Usage		   : ./install-lamp.sh
# Version	   : 0.4
# Created Date : Saturday 16 July 2022 10:46:40 PM IST
# Modified Date: Wednesday 20 July 2022 09:42:00 PM IST

SCT_VER="0.4"

# Stop the program if any error occurs
set -e

# Check if the script runs as root
function check_root() {
	if [[ $UID != 0 ]]; then
		echo -e "\n[*] ERROR:"
		echo -e "[!] Run as root (sudo $0)\n"
		exit 1
	fi
}

# Banner
function banner() {
	echo -e "\n _        _    __  __ ____    _           _        _ _           "
	echo "| |      / \  |  \/  |  _ \  (_)_ __  ___| |_ __ _| | | ___ _ __ "
	echo "| |     / _ \ | |\/| | |_) | | | '_ \/ __| __/ _\` | | |/ _ \ '__|"
	echo "| |___ / ___ \| |  | |  __/  | | | | \__ \ || (_| | | |  __/ |   "
	echo -e "|_____/_/   \_\_|  |_|_|     |_|_| |_|___/\__\__,_|_|_|\___|_|  v${SCT_VER} \n"
}

# show info
function get_info() {
	echo -e "\n[*] Starting LAMP server installation..." && sleep 2
	echo -e "\n[*] OS: $(cat /etc/os-release | head -n1 | cut -d'=' -f2)\n"
}

# Choose the version of PHP
function choose_php() {
	echo -e "[*] There are 2 versions available for PHP:\n"
	echo "   1) php7"
	echo "   2) php8"

	echo 

	read -p "==> Enter a number (default=2): " opts

	case $opts in
		1)
			echo "==> Adding php7 to installation... "
			PHP_VER="php7"
			;;
		2)
			echo "==> Adding php8 to installation... "
			PHP_VER="php"
			;;
		*)
			echo "==> Adding php8 [Default] to installation... "
			PHP_VER="php"
			;;
	esac

}

# Install packages
function install_packages() {
	# Creating a declarative numeric array
	declare -a Packages

	if [ $PHP_VER == "php7" ]; then
		Packages+=("php7")
		Packages+=("php7-apache")
	elif [ $PHP_VER == "php" ]; then
		Packages+=("php")
		Packages+=("php-apache")
	fi

	Packages+=("apache")
	Packages+=("mariadb")
	Packages+=("mariadb-clients")

	# printf "==> Installing %s\n" "${Packages[@]}"

	for i in ${Packages[@]}; do
		echo "==> Installing $i"
		pacman -S $i --noconfirm --needed
	done

}

# Start services
function start_services() {
	echo "==> Enabling services"
	# systemctl enable --now httpd.service
	# systemctl enable --now mariadb.service
}

# Setup database
function setup_database() {
	# Install the MariaDB data directory
	# Remove everything in data directory
	if [[ -d /var/lib/mysql ]]; then
		echo "==> Removing mariadb directory"
		rm -r /var/lib/mysql/*
	fi
	echo "==> Installing MariaDB directory in /var/lib/mysql"
	# mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

	# Securing MariaDB using mysql_secure_installation
	echo "==> Securing MariaDB using mysql_secure_installation"
	# mysql_secure_installation
}

function configure_php() {
	local php_config
	php_config="/etc/${PHP_VER}/php.ini"
}

function configure_apache() {
	local apache_config
	apache_config="/etc/httpd/conf/httpd.conf"
}


# Run functions
banner
# check_root
get_info
choose_php
install_packages
